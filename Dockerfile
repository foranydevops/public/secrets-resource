FROM alpine:latest as builder

RUN apk update \
    && apk add curl

RUN curl -Lo /usr/local/bin/sops https://github.com/getsops/sops/releases/download/v3.8.1/sops-v3.8.1.linux.amd64 \
    && chmod a+x /usr/local/bin/sops

RUN curl -Lo /usr/local/bin/secrets-manager https://gitlab.com/foranydevops/public/secrets-manager/-/raw/main/bin/secrets-manager \
    && chmod a+x /usr/local/bin/secrets-manager

RUN curl -fsSL -o gum.tar.gz https://github.com/charmbracelet/gum/releases/download/v0.14.0/gum_0.14.0_Linux_x86_64.tar.gz \
    && tar -C /usr/local -xzf gum.tar.gz

FROM alpine:latest

COPY --from=builder /usr/local/bin/sops /usr/local/bin/sops
COPY --from=builder /usr/local/bin/secrets-manager /usr/local/bin/secrets-manager
COPY --from=builder /usr/local/gum_0.14.0_Linux_x86_64/gum /usr/local/bin/gum

RUN apk update && apk add --update bash jq coreutils gpg gpg-agent git openssh-client && mkdir /opt/resource \
    && chmod +x /usr/local/bin/secrets-manager /usr/local/bin/sops /usr/local/bin/gum

COPY bin/ /opt/resource

RUN chmod a+x /opt/resource/in /opt/resource/out /opt/resource/check /opt/resource/common.sh