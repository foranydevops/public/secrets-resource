#!/bin/bash

load_pubkey() {
  local private_key_path=$TMPDIR/secrets-resource-store-private-key
  local private_key_user=$(jq -r '.source.private_key_user // empty' <<< "$1")
  local forward_agent=$(jq -r '.source.forward_agent // false' <<< "$1")

  (jq -r '.source.private_key // empty' <<< "$1") > $private_key_path

  if [ -s $private_key_path ]; then
    chmod 0600 $private_key_path

    # create or re-initialize ssh-agent
    init_ssh_agent

    DISPLAY= ssh-add $private_key_path > /dev/null

    mkdir -p ~/.ssh
    cat > ~/.ssh/config <<EOF
StrictHostKeyChecking no
LogLevel quiet
EOF
    if [ ! -z "$private_key_user" ]; then
      cat >> ~/.ssh/config <<EOF
User $private_key_user
EOF
    fi
    if [ "$forward_agent" = "true" ]; then
      cat >> ~/.ssh/config <<EOF
ForwardAgent yes
EOF
    fi
    chmod 0600 ~/.ssh/config
  fi
}

init_ssh_agent() {

  # validate if ssh-agent exist
  set +e
  ssh-add -l &> /dev/null
  exit_code=$?
  set -e
  
  if [[ ${exit_code} -eq 2 ]]; then
    # ssh-agent does not exist, create ssh-agent
    eval $(ssh-agent) > /dev/null 2>&1
    trap "kill $SSH_AGENT_PID" EXIT
  else
    # ssh-agent exist, remove all identities
    ssh-add -D &> /dev/null
  fi
}

add_git_metadata_basic() {
  local commit=$(git rev-parse HEAD | jq -R .)
  local author=$(git log -1 --format=format:%an | jq -s -R .)
  local author_date=$(git log -1 --format=format:%ai | jq -R .)

  jq ". + [
    {name: \"commit\", value: ${commit}},
    {name: \"author\", value: ${author}},
    {name: \"author_date\", value: ${author_date}, type: \"time\"}
  ]"
}

add_secrets_params_basic() {
  local uri_store=`echo $1 | jq -r '.source.uri'`
  local org=`echo $1 | jq -r '.params.org'`
  local target_ssl_key=`echo $1| jq -r '.params.target_ssl'`
  local target_ssl_crt=`echo $1 | jq -r '.params.target_crt'`
  local target_ssl_ca=`echo $1 | jq -r '.params.target_ca'`
  local target_kubeconfig=`echo $1 | jq -r '.params.target_kubeconfig'`
  local target_general_secrets=`echo $payload | jq -r '.params.general_secrets'`

  jq ". + [
    {name: \"store\", value: \"git\"},
    {name: \"store_uri\", value: \"${uri_store}\"},
    {name: \"organization\", value: \"${org}\"},
    {name: \"target_ssl_key\", value: \"${target_ssl_key}\"},
    {name: \"target_ssl_crt\", value: \"${target_ssl_crt}\"},
    {name: \"target_ssl_ca\", value: \"${target_ssl_ca}\"},
    {name: \"target_kubeconfig\", value: \"${target_kubeconfig}\"},
    {name: \"general_secrets\", value: \"${target_general_secrets}\"}
  ]"
}

secrets_metadata() {
  jq -n "[]" | \
    add_git_metadata_basic | \
    add_secrets_params_basic "$1"
}

load_pgp_key() {
    local pgp_first_key_path=$TMPDIR/secrets-resource-pgp-first-key
    local pgp_second_key_path=$TMPDIR/secrets-resource-pgp-second-key

    (jq -r '.source.pgp_first_key // empty' <<< "$1") > $pgp_first_key_path
    (jq -r '.source.pgp_second_key // empty' <<< "$1") > $pgp_second_key_path

    secrets-manager import-keys $pgp_first_key_path
    secrets-manager import-keys $pgp_second_key_path
}

validate_empty() {
    arg2="${2:-blank}"
    if [[ "$arg2" == 'null' ]] || [[ $arg2 == 'blank' ]]; then
        log error "$1 was not informed"
        return 1
    fi
}

check_param() {
    arg="${1:-blank}"
    if [[ "$arg" == 'null' ]] || [[ $arg == 'blank' ]]; then
        return 1
    fi
}

log() {
    level=${1:-info}
    gum log --time="rfc822" --level=$level ${@: 2}
}

run() {
    (${@})
}
